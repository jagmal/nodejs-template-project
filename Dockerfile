# nodejs-template-project-prod image for our sample app

# Run following command from the current directory
# docker build -t nodejs-template-project-prod .
FROM node:10.15.3-alpine as base-image

# We want to use nodejs user and not default root
# RUN groupadd -r nodejs && useradd -m -r -g nodejs nodejs
# USER nodejs

# Create app directory
RUN mkdir -p /var/app/nodejs-template-project

WORKDIR /var/app/nodejs-template-project

# Install app dependencies
COPY package.json package-lock.json ./

RUN npm install --production

# Bundle app source
COPY . /var/app/nodejs-template-project

FROM base-image as prod

WORKDIR /var/app/nodejs-template-project
ENV NODE_ENV production
EXPOSE 3000
CMD [ "npm", "start" ]


FROM base-image as dev

WORKDIR /var/app/nodejs-template-project
ENV NODE_ENV development
RUN npm config set strict-ssl false
RUN npm install -g nodemon@1.18.11
RUN npm install
EXPOSE 3000
EXPOSE 9229
CMD [ "npm", "run", "dev-old" ]
