# nodejs-template-project

Template project for building microservices in NodeJS.

Idea is to create a template project that covers following (and more) concepts
related to writing a micro service in NodeJS:
  - Javascript Coding Guidelines
  - Logging
  - DB handling
  - Documentation
  - Authorization
  - Configuration Management
  - Tests (Unit and Behavioral)
  - Debugging
  - Docker Images
  - CI/CD pipeline
  - Kubernetes for production cluster
  - Editor Setup

  Our aim is to be as opinionated as we can be and recommend best practices that
  we want to adhere to at organizational level.

Notes:
--

Basic setup with docker
---
We are using nodeJS version 10.15.3 which is the current
LTS version and also support ES6 syntax. For ES6 syntax, we had to add babel in
dev dependencies and add "dev" target inside scripts. This "dev" target gets
initiated in the dev docker image.

Useful links:

https://medium.freecodecamp.org/how-to-enable-es6-and-beyond-syntax-with-node-and-express-68d3e11fe1ab


Debugging with VS Code:
---

While setting up the dev docker image, we exposed port 9229, which is the
default debug port for node. Then, we mapped it to 5858 port (we can choose
custom port for every image). VS Code needs to connect have a remote debug
config set to connect to the port we have opened on local machine. Also, don't
forget to allow connection to the docker image debug mode from any machine as
the host machine won't be allowed by default.


Some links:


https://blog.risingstack.com/how-to-debug-a-node-js-app-in-a-docker-container/
https://nodejs.org/en/docs/guides/debugging-getting-started/


Container on Local Development Machines
---

For local development, we would be using docker-compose to deploy the docker
container for the current service and also dependencies required. This would mean
that if there are multiple micro-services dependent on each other, each one
will have a separate docker-compose file and they would depend on the image for
the dependency.

Next question to address is - what happens when version/configuration for a
micro-service change? Would all dependent services have to make a change?

Short answer is - Yes. But, it also gives the downstream systems flexibility to
decide what version of the image they want to continue using. Auto-magically
expecting all downstream services to start using latest build is unfair and
discouraged.


ToDos (sample code for):
--

[x] Research on how should k8s dev to prod setup pipeline be
[x] Setup docker compose (if that's the right way to setup k8s)
- integrating eslint
- writing sample APIs with right folder structure
- writing documentation for sample APIs
- writing test cases for sample APIs
- setting up CI/CD pipeline
- deployment to production (and related changes)
- Typescript support (?)
- Authentication using JWT
- Rate Limiting
- Versioning of docker images

