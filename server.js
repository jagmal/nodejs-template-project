'use strict';

let express = require('express');
// import express from 'express';

// Constants
const PORT = 3000;
const HOST = '0.0.0.0';

// App
const app = express();
app.get('/', (req, res) => {
  console.log('here');
  res.send('Hello World\n');
});

app.listen(PORT, HOST);
console.log(`Running -- on http://${HOST}:${PORT}`);
